# Databricks notebook source
# MAGIC %md
# MAGIC # Nasa Exercise

# COMMAND ----------

from pyspark.sql import *
from pyspark.sql.functions import *


# COMMAND ----------

# Leemos la tabla y la transformamos en un dataframe
df = spark.read.table('nasa_table')

# Creamos todas las columnas
df = df.withColumn('host', split(df['column'], ' ').getItem(0)) \
    .withColumn('user_identifier', split(df['column'], ' ').getItem(1)) \
    .withColumn('user_id', split(df['column'], ' ').getItem(2)) \
    .withColumn('date', split(df['column'], ' ').getItem(3)) \
    .withColumn('time_zone', split(df['column'], ' ').getItem(4)) \
    .withColumn('request_method', split(df['column'], ' ').getItem(5)) \
    .withColumn('resource', split(df['column'], ' ').getItem(6)) \
    .withColumn('protocol', split(df['column'], ' ').getItem(7)) \
    .withColumn('http_status_code', split(df['column'], ' ').getItem(8)) \
    .withColumn('size', split(df['column'], ' ').getItem(9)) \

#     .withColumn('all_request', split(df['column'], '"').getItem(1))
# df = df.withColumn(
#     "request_method",
#     split(regexp_replace("all_request", "\w*", r"$1,"), ",")
# )

# # Separamos la columna de la fecha en día, mes y año
df = df.withColumn('day', split(df['date'], '/').getItem(0)) \
    .withColumn('month', split(df['date'], '/').getItem(1)) \
    .withColumn('year', split(df['date'], '/').getItem(2)) 

# # Separamos la columna de la hora en hora, minutos y segundos
df = df.withColumn('hour', split(df['year'], ':').getItem(1)) \
    .withColumn('minutes', split(df['year'], ':').getItem(2)) \
    .withColumn('seconds', split(df['year'], ':').getItem(3)) \
    .withColumn('year', split(df['year'], ':').getItem(0))

# # Eliminamos los caracteres que sobran
df = df.withColumn('request_method', regexp_replace('request_method', '"', ''))
df = df.withColumn('day', regexp_replace('day', '\[', ''))
df = df.withColumn('time_zone', regexp_replace('time_zone', ']', ''))
df = df.withColumn('time_zone', regexp_replace('time_zone', '-', ''))
df = df.withColumn('protocol', regexp_replace('protocol', '"', ''))
    
# # Eliminamos las columnas que sobran
df = df.drop('column')
df = df.drop('date')



df.show(truncate=False)

# COMMAND ----------

# Se recastean todas las columnas para establecer el tipo correcto
df = df.selectExpr(
    "cast(host as string) host",
    "cast(user_identifier as string) user_identifier",
    "cast(user_id as string) user_id",
    "cast(time_zone as string) time_zone",
    "cast(request_method as string) request_method",
    "cast(resource as string) resource",
    "cast(protocol as string) protocol",
    "cast(http_status_code as int) http_status_code",
    "cast(size as int) size",
    "cast(day as int) day",
    "cast(month as string) month",
    "cast(year as int) year",
    "cast(hour as int) hour",
    "cast(minutes as int) minutes",
    "cast(seconds as int) seconds"
)

# COMMAND ----------

# Comprobamos el schema del dataframe para ver si está todo correcto
df.printSchema()

# COMMAND ----------

# Guardamos el dataframe como tabla
df.write.mode("overwrite").saveAsTable("nasa_table_prep")

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Distintos protocolos utilizados
# MAGIC SELECT DISTINCT(protocol)
# MAGIC FROM nasa_table_prep

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Códigos de estado más comunes
# MAGIC SELECT http_status_code, COUNT(http_status_code) AS n_ocurrences
# MAGIC FROM nasa_table_prep
# MAGIC GROUP BY http_status_code
# MAGIC ORDER BY COUNT(http_status_code) DESC

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Métodos de petición más comunes
# MAGIC SELECT request_method, COUNT(request_method) AS n_ocurrences
# MAGIC FROM nasa_table_prep
# MAGIC GROUP BY request_method
# MAGIC ORDER BY COUNT(request_method) DESC

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Recursos con más transferencia de bytes
# MAGIC SELECT resource, SUM(size) AS sum_size
# MAGIC FROM nasa_table_prep
# MAGIC GROUP BY resource
# MAGIC ORDER BY SUM(size) DESC

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Recurso con más registros en nuestro log
# MAGIC SELECT resource, COUNT(resource) AS n_ocurrences
# MAGIC FROM nasa_table_prep
# MAGIC GROUP BY resource
# MAGIC ORDER BY COUNT(resource) DESC
# MAGIC LIMIT 1

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Días en los que se recibió más tráfico
# MAGIC SELECT concat(day, ' ', month, ' ', year) AS date, SUM(size) AS sum_size
# MAGIC FROM nasa_table_prep
# MAGIC GROUP BY date
# MAGIC ORDER BY SUM(size) DESC

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Hosts más frecuentes
# MAGIC SELECT host, COUNT(host) AS n_ocurrences
# MAGIC FROM nasa_table_prep
# MAGIC GROUP BY host
# MAGIC ORDER BY COUNT(host) DESC

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Hora con más tráfico
# MAGIC SELECT hour, SUM(size) AS sum_size
# MAGIC FROM nasa_table_prep
# MAGIC GROUP BY hour
# MAGIC ORDER BY SUM(size) DESC

# COMMAND ----------

# MAGIC %sql
# MAGIC -- Número de errores 404 cada día
# MAGIC SELECT concat(day, ' ', month, ' ', year) AS date, COUNT(http_status_code) AS n_ocurrences
# MAGIC FROM nasa_table_prep
# MAGIC WHERE http_status_code LIKE '404'
# MAGIC GROUP BY date
